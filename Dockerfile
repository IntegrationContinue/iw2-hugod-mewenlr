FROM composer:latest AS composer

COPY composer.json /code/composer.json
COPY composer.lock /code/composer.lock

WORKDIR /code

RUN composer install --no-dev --classmap-authoritative --prefer-dist --ignore-platform-reqs --no-scripts

FROM php:7.2-apache

ENV APP_ENV=prod

RUN docker-php-ext-install pdo pdo_mysql \
 && a2enmod rewrite \
 && sed -i 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf

COPY --from=composer /code/vendor /var/www/html/vendor


WORKDIR /var/www/html
